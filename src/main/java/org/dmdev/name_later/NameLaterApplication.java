package org.dmdev.name_later;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NameLaterApplication {

    public static void main(String[] args) {
        SpringApplication.run(NameLaterApplication.class, args);
    }

}

package org.dmdev.name_later.controllers;

import org.dmdev.name_later.models.ImageModel;
import org.dmdev.name_later.models.ResponseModel;
import org.dmdev.name_later.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/images")
public class ImageController {
    @Autowired
    private ImageService imageService;

    @GetMapping("")
    public ResponseEntity<ResponseModel> getAll(){
        return new ResponseEntity<>(imageService.getAll(), HttpStatus.OK);
    }

    @PutMapping("")
    public ResponseEntity<ResponseModel> save(@RequestBody ImageModel imageModel){
        return new ResponseEntity<>(imageService.save(imageModel), HttpStatus.CREATED);
    }

    @PostMapping("")
    public ResponseEntity<ResponseModel> update(@RequestBody ImageModel imageModel){
        return new ResponseEntity<>(imageService.update(imageModel), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseModel> delete(@PathVariable Long id){
        return new ResponseEntity<>(imageService.delete(id), HttpStatus.OK);
    }
}

package org.dmdev.name_later.repositories;

import org.dmdev.name_later.entities.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageHibernateDAO extends JpaRepository<Image, Long> {
}

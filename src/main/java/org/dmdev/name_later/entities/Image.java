package org.dmdev.name_later.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(nullable = false,
    columnDefinition = "TEXT")
    String image;
    @Column(nullable = false,
            columnDefinition = "TEXT")
    String original;
    @Column(nullable = false,
            columnDefinition = "DATE",
            name = "load_date")
    LocalDate load;
    @Column(nullable = false,
    columnDefinition = "TEXT")
    String translate;
}

package org.dmdev.name_later.services.interfaces;

import org.dmdev.name_later.models.ImageModel;
import org.dmdev.name_later.models.ResponseModel;

public interface IImageService {

    ResponseModel save(ImageModel imageModel);
    ResponseModel update(ImageModel imageModel);
    ResponseModel getAll(Long id);
    ResponseModel delete(Long id);
}

package org.dmdev.name_later.services;

import org.dmdev.name_later.entities.Image;
import org.dmdev.name_later.models.ImageModel;
import org.dmdev.name_later.models.ResponseModel;
import org.dmdev.name_later.repositories.ImageHibernateDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ImageService {
    @Autowired
    private ImageHibernateDAO imageDAO;

    public ResponseModel save(ImageModel imageModel) {
        Image image = Image.builder()
                .image(imageModel.getImage())
                .original(imageModel.getOriginal())
                .load(imageModel.getLoad())
                .translate(imageModel.getTranslate())
                .build();
        imageDAO.save(image);
        System.out.println("Image Saved");
        return ResponseModel.builder()
                .status(ResponseModel.SUCCESS_STATUS)
                .message(String.format("Image %d Saved", image.getId()))
                .build();
    }

    public ResponseModel update(ImageModel imageModel) {
        Optional<Image> imageOpt = imageDAO.findById(imageModel.getId());
        if (imageOpt.isPresent()) {
            Image image = Image.builder()
                    .id(imageModel.getId())
                    .image(imageModel.getImage())
                    .original(imageModel.getOriginal())
                    .load(imageModel.getLoad())
                    .translate(imageModel.getTranslate())
                    .build();
            imageDAO.delete(image);
            return ResponseModel.builder()
                    .status(ResponseModel.SUCCESS_STATUS)
                    .message(String.format("Image %d Updated", image.getId()))
                    .build();
        } else {
            return ResponseModel.builder()
                    .status(ResponseModel.SUCCESS_STATUS)
                    .message(String.format("Image With Id {%d} Does Not Exist", imageModel.getId()))
                    .build();
        }
    }

    public ResponseModel getAll() {
        List<Image> images = imageDAO.findAll(Sort.by("id").descending());
        List<ImageModel> imageModels =
                images.stream().map(i ->
                    ImageModel.builder()
                            .image(i.getImage())
                            .original(i.getOriginal())
                            .load(i.getLoad())
                            .translate(i.getTranslate())
                            .build()
                ).toList();
        return ResponseModel.builder()
                .status(ResponseModel.SUCCESS_STATUS)
                .data(imageModels)
                .build();

    }

    public ResponseModel delete(Long id) {
        Optional<Image> imageOpt = imageDAO.findById(id);
        if (imageOpt.isPresent()) {
            Image image = imageOpt.get();
            imageDAO.delete(image);
            return ResponseModel.builder()
                    .status(ResponseModel.SUCCESS_STATUS)
                    .message(String.format("Image {%d} Deleted", id))
                    .build();
        } else {
            return ResponseModel.builder()
                    .status(ResponseModel.SUCCESS_STATUS)
                    .message(String.format("Image With Id {%d} Does Not Exist", id))
                    .build();
        }
    }
}


